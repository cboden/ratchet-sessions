#### Major WIP

Consider using Promises for refreshing/refetching the data

I think the Provider deals with the Ratchet interface stuff and the Adapter deals with the runner?
A runner may require a transport? ZeroMQ, Stream/RFC6455
Runners could include ChildProcess, Fork, PThread?

I think the session object may need to be wrapped by a Ratchet object?
What if it's just an array?
Symfony -> HttpFoundation\Session\Session, Aura -> Session\Segment
Symfony save() closes the session - it will have to be wrapped

Each Connections's Session object needs a fetch() and save() method at the minimum, as well as a way to access the data.
Perhaps the fetch() could return the Adapter Session object/array? The save() would have to accept one as well?
Promise's would be returned.
This Ratchet ConnSess would talk to the runner and be aware of the connection?
The receiver only needs the session ID.


onConstruct - setup the runner, load session options(?), create the serialize handler
onOpen - attach a session object - do not load any sessions
onClose - cleanup resources
onAnything - session object can load() or save() talking to the runner returning something from the adapter?
