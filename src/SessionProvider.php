<?php
namespace Ratchet\Sessions;
use Ratchet\Sessions\Adapter\SessionAdapterInterface;
use Ratchet\Sessions\Runner\SessionRunnerInterface;
use Ratchet\Http\HttpServerInterface;
use React\EventLoop\EventLoopInterface;

class SessionProvider implements HttpServerInterface {
    /**
      * @var \Ratchet\Http\HttpServerInterface
      */
    protected $_app;

    /**
     * Null storage handler if no previous session was found
     * @var \SessionHandlerInterface protected $_null;
     */

    /**
     * @var \Ratchet\Sessions\Serialize\HandlerInterface
     */
    protected $_serializer;

    /**
     * @var \React\EventLoop\EventLoopInterface
     */
    protected $_loop;

    public function __construct(HttpServerInterface $app, SessionRunnerInterface $runner, SessionAdapterInterface $adapter) {
    }
}
