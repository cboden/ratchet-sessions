<?php
namespace Ratchet\Sessions\Session;
use React\Promise\Deferred;

class Session {
    /**
     * @var Ratchet\Session\Adapter\SessionAdapterInterface
     */
    protected $_adapter;

    public function __construct(SessionAdapterInterface $adapter, $sessionId) {
    }

    /**
     * @returns React\Promise\Promise
     */
    public function load() {
        $defer = new React\Promise\Deferred;

        // talk to adapter

        return $defer->promise();
    }

    /**
     * @returns React\Promise\Promise
     */
    public function save() {
    }
}
